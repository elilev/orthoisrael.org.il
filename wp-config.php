<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */
//
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'orthoisrael' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );


define('WP_HOME', 'http://dev.ortho.local/');
define('WP_SITEURL', 'http://dev.ortho.local/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Y,1DAg(EGmO:^yB(N.eY+5pz8)HRB%qw@dDb-?qAsv,cn%&&QDm8%NFGNE>JTem*' );
define( 'SECURE_AUTH_KEY',  '=f*6Ugis@MT][u?j&9UNNn&r^H=ME$=W41vJ.qy:2k(2HPc%<Uy{*Qf+|rk[=O2w' );
define( 'LOGGED_IN_KEY',    '^>*Vh=5w<slh}tJIMF/WYbp[&]qw#!jL)1b.wP=g RWc7@o{Y7~j<_$4uPL85J@/' );
define( 'NONCE_KEY',        'pYUozP/0CM?MGkYl,_Y[sq_  )GU_p|D|3y.Ih;#G/wm,xFfBJItLCv(/^m47w]m' );
define( 'AUTH_SALT',        'D$qC/s8{-LF Lz/?Sw5!>E%HKskx}v]<QZ{Y&`tT=HcM?GJD4CG9+0EXt9;bTwND' );
define( 'SECURE_AUTH_SALT', 'ZQ.^):P7ZsJ=3C$4b0j_8DwpHC%0nqB4:U2EaDcm}_?jnP}}?Q;@BQXFE(86I{,B' );
define( 'LOGGED_IN_SALT',   'IBg3?WVW-dm0iJr7.x:cfAV|rTIN,Z(dwrq6t4~9aFV@z0+G3UYCz2@9m9Zgk6Cd' );
define( 'NONCE_SALT',       '[4d,ogK5nJdt V}B#c>Z!%#gqO4np Iq%AyhQ-)p_Ie;?3<R!s^$P#RAWr1Bk/4^' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpoi_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
