<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Template Name: Qna
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();

get_header();
$height = 'tall';
include( locate_template( 'template-parts/page-top.php', false, false ) );
?>
<div class="grid-container">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area offset small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        <div class="large-4 cell side-bar">
            <?php  get_template_part('template-parts/sidebar-menu'); ?>    
        </div>
        <div class="large-8 cell">
            <div class="content-block"> 
                <?php 
                    if( have_rows('accordion') ):
                        while( have_rows('accordion') ) : the_row();
                            $title = get_sub_field('title');
                            $accordion = get_sub_field('accordion');
                            ?>
                            <div class="acc-group">
                                <h2> <?php echo $title; ?> </h2>
                                <ul class="accordion" data-accordion  data-allow-all-closed="true">
                                <?php foreach($accordion as $acc){ ?>
                                    <li class="accordion-item " data-accordion-item >
                                        <a href="#" class="accordion-title"><?php echo $acc['title']; ?></a>
                                        <div class="accordion-content" data-tab-content>
                                        <p><?php echo $acc['content']; ?></p>
                                        </div>
                                    </li>     
                                <?php } ?>
                                </ul>
                            </div>
                            <?php
                        endwhile;
                    endif;
                ?>
            </div>
        </div>

       

    </div>
    
    
               
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();
