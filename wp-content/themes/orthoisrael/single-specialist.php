<?php
/**
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();

get_header();
$height = 'short';
include( locate_template( 'template-parts/page-top.php', false, false ) );
$fields = get_fields();
?>
<div class="grid-container single">
<div id="content" class="site-content grid-x grid-padding-x " >
<div id="primary" class="content-area  small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-margin-x align-right m-40 update-card-link">
        <div class="shrink cell">
            <a href="#" id="update-card-link"><i class="material-icons">help</i><span><?php _e('Update personal card', 'podium'); ?></span></a>
        </div>
    </div>
    <div class="grid-x grid-margin-x align-justify">
        <div class="large-4 cell">
        <aside>
        <?php if(has_post_thumbnail()){
            the_post_thumbnail('thumbnail');
        }else{
            echo '<img src="' . get_template_directory_uri().'/dist/images/avatar.png">';
        }

        ?>
                <h3><?php the_title(); ?> </h3>
                <?php if($fields['licence_number']){ ?>
                    <div>
                        <span> <?php _e('Dentist licence number','podium'); ?>: </span>
                        <p><?php echo $fields['licence_number']; ?></p>
                    </div>
                <?php } ?>
                <?php if($fields['specialist_number']){ ?>
                    <div>
                        <span> <?php _e('Specialist licence number','podium'); ?>: </span>
                        <p><?php echo $fields['specialist_number']; ?></p>
                    </div>
                <?php } ?>
                <?php if($fields['clinics']){ ?>
                    <div>
                        <span> <?php _e('Clinics','podium'); ?>: </span>
                        <p><?php echo $fields['clinics']; ?></p>
                    </div>
                <?php } ?>
                <?php if($fields['website']){ ?>
                    <div>
                        <span> <?php _e('Website','podium'); ?>: </span>
                        <p><a href="<?php echo $fields['website']; ?>" target="_blank"><?php echo $fields['website']; ?></a></p>
                    </div>
                <?php } ?>
                <?php if($fields['email']){ ?>
                    <a  href="mailto:<?php echo $fields['email']; ?>" class="contact-specialist">
                        <i class="material-icons">email</i> <?php _e('Contact me','podium'); ?>
                    </a>
                <?php } ?>
        </aside>
        </div>
        <div class="large-8 cell">
        <div class="specialist-content">            
            <article class="pxy-2">
                <div class="entry-content">
                <h3 class="title"><?php _e('About me', 'podium'); ?> </h3>
                    <?php the_content(); ?>
                </div>
            </article>
        </div>
        <div class="specialist-form m-40">       
            <?php  $update_card_form = (get_field('update_card_form','option_general_' . ICL_LANGUAGE_CODE)) ? get_field('update_card_form','option_general_' . ICL_LANGUAGE_CODE) : get_field('update_card_form','option_general_all'); ?>     
            <article class="pxy-2">
            <h3 class="title"><?php echo $update_card_form['title']; ?></h3>
            <p><?php echo $update_card_form['text']; ?></p>
            <div> <?php echo do_shortcode( $update_card_form['form'] ); ?>
            </article>
        </div>
        </div>
        
    </div>

   
                       
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();