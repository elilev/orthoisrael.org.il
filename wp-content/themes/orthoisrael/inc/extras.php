<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package podium
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param  array   $classes Classes for the body element.
 * @return array
 */
function podium_body_classes($classes)
{

// Adds a class of group-blog to blogs with more than 1 published author.
    if (is_multi_author()) {

        $classes[] = 'group-blog';

    }

    return $classes;
}

add_filter('body_class', 'podium_body_classes');

// Get post Thumb URL
/**
 * @param  $post
 * @param  $size
 * @return mixed
 */
function get_thumb_url($post, $size = 'full')
{

    if (has_post_thumbnail($post->ID)) {

        $attachment_id = get_post_thumbnail_id($post->ID);

        // thumbnail, medium, large, or full
        $src = wp_get_attachment_image_src($attachment_id, $size);
        return $src[0];

    }

    return false;
}

/**
 * @param  $limit
 * @return string
 */
function excerpt($limit)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);

    if (count($excerpt) >= $limit) {

        array_pop($excerpt);
        $excerpt = implode(' ', $excerpt);

    } else {

        $excerpt = implode(' ', $excerpt);

    }

    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

if (!function_exists('floating_direction')) {
    /**
     * @param $reverse
     */
    function floating_direction($reverse = false)
    {

        if ((is_rtl() && !$reverse) || (!is_rtl() && $reverse)) {
            return 'right';
        } else {
            return 'left';
        }

    }

}


function clean($value){
    $result = trim(strip_tags($value));
    $result = str_replace(array('\'', '"'), '', $result); 
    $result = stripslashes( $result);
    return $result;
}

function get_the_content_clean($limit){
    $content = wp_strip_all_tags( get_the_content()) ; 
    $regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@";
    return wp_trim_words(preg_replace($regex, ' ', $content),$limit);
}


function get_specialist(){

    $term_id = $_POST['termId'];
    $specialist_id = $_POST['specialistID'];

    
    $args = array(
        'post_type' => 'specialist',
        'posts_per_page' => '-1',
        // 's' => $search_query
        
        'orderby' => 'slug',
        'order'    =>'ASC',
        'post_status' => 'publish'

    );

    if(is_array( $specialist_id)){
        $specialist_id_arr = explode(',' ,$specialist_id);
        $args['post__in']  = $specialist_id_arr;
    }else{
        $args['p']  = $specialist_id;
    }


    if($term_id!='all'){
        $args['tax_query'][] =  array (
            'taxonomy' => 'location',
            'field' => 'term_id',
            'terms' =>$term_id,
        );
       
    }
    $spe_query = new wp_query( $args );
     if (  $spe_query->have_posts() ) {
        while (  $spe_query->have_posts() ) {  $spe_query->the_post();
           
         get_template_part('template-parts/card');         
        }
    } 
   
    
   
    die();
}
add_action( 'wp_ajax_nopriv_get_specialist', 'get_specialist' );
add_action( 'wp_ajax_get_specialist', 'get_specialist' );

// change author of comments if logged in
add_filter( 'preprocess_comment' , 'preprocess_comment_meta_edit' );
function preprocess_comment_meta_edit( $commentdata ) {
   $commentdata['comment_author'] = $_POST['author'];
   return $commentdata;
}


function modify_product_cat_query( $query ) {
    if (!is_admin() && $query->is_tax("discussion-type")){
         $query->set('posts_per_page',10);
    }
  }
  add_action( 'pre_get_posts', 'modify_product_cat_query' );