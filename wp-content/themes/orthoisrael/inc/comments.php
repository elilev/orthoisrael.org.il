<?php

// Comment Layout
/**
 * @param $comment
 * @param $args
 * @param $depth
 */


function podium_comment($comment, $args, $depth) {
  $GLOBALS['comment'] = $comment;
  extract($args, EXTR_SKIP);

?>
<li <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
  <div id="div-comment-<?php comment_ID() ?>" class="comment-body">

	  <?php if ( $comment->comment_approved == '0' ) : ?>
	    <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
	    <br />
	  <?php endif; ?>
	    
	  <div class="comment-meta commentmetadata ">   
			<span class="comment-name">
				<?php echo  get_comment_author_link() ?>	
			</span> 
			 <span class="comment-date"> 
			 	<?php echo  get_comment_date( "d.m.y"); ?>
			 </span>
			 <!-- <span class="">  
			 	<?php edit_comment_link( __( '(Edit)' ), '  ', '' ); ?>
			 </span> -->
		
			  
		</div>

	  <div class="the-comment">  
	  <?php comment_text(); ?>
	  </div>
	   <div class="reply">
	      <?php comment_reply_link( array_merge( $args, array( 'add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
	    </div>
		
	</div>

</li>

<?php
}

?>
