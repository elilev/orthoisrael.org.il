<?php
// This file handles the admin area and functions - You can use this file to make changes to the dashboard.

/************* DASHBOARD WIDGETS *****************/
// Disable default dashboard widgets
function disable_default_dashboard_widgets()
{

    remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Comments Widget
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');  // Incoming Links Widget
    remove_meta_box('dashboard_plugins', 'dashboard', 'core');         // Plugins Widget
    remove_meta_box('dashboard_quick_press', 'dashboard', 'core');     // Quick Press Widget
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');   // Recent Drafts Widget
    remove_meta_box('dashboard_primary', 'dashboard', 'core');
    remove_meta_box('dashboard_secondary', 'dashboard', 'core');
    remove_meta_box('yoast_db_widget', 'dashboard', 'normal'); // Yoast's SEO Plugin Widget
}

// removing the dashboard widgets
add_action('admin_menu', 'disable_default_dashboard_widgets');

/************* CUSTOMIZE ADMIN *******************/

// Custom Backend Footer
function podium_custom_admin_footer()
{
    $wp_v         = get_bloginfo('version');
    $w_copyrights = _x('<span id="footer-thankyou">Developed by <a href="http://elilev.me" target="_blank">Weepo</a></span>', 'podium');
    return '<span style="direction:lrt;">v' . $wp_v . ' | ' . $w_copyrights . '</span>';
}

// adding it to the admin area
add_filter('admin_footer_text', 'podium_custom_admin_footer');

//ADD featured image thumbnail to WordPress admin columns

add_filter('manage_posts_columns', 'tcb_add_post_thumbnail_column', 5);
add_filter('manage_pages_columns', 'tcb_add_post_thumbnail_column', 5);

/**
 * @param  $cols
 * @return mixed
 */
function tcb_add_post_thumbnail_column($cols)
{
    $cols['tcb_post_thumb'] = __('Main image');
    return $cols;
}

add_action('manage_posts_custom_column', 'tcb_display_post_thumbnail_column', 5, 2);
add_action('manage_pages_custom_column', 'tcb_display_post_thumbnail_column', 5, 2);

/**
 * @param $col
 * @param $id
 */
function tcb_display_post_thumbnail_column($col, $id)
{

    switch ($col) {
        case 'tcb_post_thumb':

            if (function_exists('the_post_thumbnail')) {
                echo the_post_thumbnail('thumbnail');
            } else {
                echo 'Not supported in theme';
            }

            break;
    }

}

// Allow svg upload in media
/**
 * @param  $mimes
 * @return mixed
 */
function cc_mime_types($mimes)
{
    $mimes['svg']  = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');


// Set interval beween heartbewats
/**
 * @param  $settings
 * @return mixed
 */
function podium_heartbeat_settings($settings)
{
    $settings['interval'] = 60; //Anything between 15-60
    return $settings;
}

add_filter('heartbeat_settings', 'podium_heartbeat_settings');


/**************************************************************/
/****************  add_options_page ***************************/
/**************************************************************/

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
      'page_title'  => __('Options','eltheme'),
      'menu_title'  =>  __('Options','eltheme'),
      'menu_slug'   => 'theme-options-settings',
      'capability'  => 'edit_posts',
      'redirect'    => true
    ));
    acf_add_options_page(array(
      'page_title'  =>  __('General','eltheme'),
      'menu_title'  =>  __('General','eltheme'),
      'menu_slug'   => 'theme-general-settings',
      'capability'  => 'edit_posts',
      'redirect'    => false,
      'parent_slug' => 'theme-options-settings',
      'post_id'     => 'option_general_' .ICL_LANGUAGE_CODE,
    ));
    acf_add_options_page(array(
      'page_title'  =>  __('Footer','eltheme'),
      'menu_title'  =>  __('Footer','eltheme'),
      'menu_slug'   => 'theme-footer-settings',
      'capability'  => 'edit_posts',
      'redirect'    => false,
      'parent_slug' => 'theme-options-settings',
      'post_id'     =>  'option_footer_'.ICL_LANGUAGE_CODE,
      
    ));
  }


  

// Disable Gutenberg
  add_filter('use_block_editor_for_post', '__return_false', 10);



  // Add forum_manage rrole
function add_forum_manager_role() {
    add_role('forum_manager',
        'Forum manager',
        array(
            'read' => true,
            'edit_posts' => false,
            'delete_posts' => false,
            'publish_posts' => false,
            'upload_files' => true,
        )
    );
}
add_action( 'init', 'add_forum_manager_role' );

function ortho_add_role_caps() {
    // Set up press team caps
    // Add the roles you'd like to administer the custom post types
    $roles = array('forum_manager', 'editor', 'administrator');
    // Loop through each role and assign capabilities
    foreach ($roles as $the_role) {

        $role = wp_roles()->get_role($the_role);

        $role->add_cap('read_ortho_discussion');
        $role->add_cap('read_private_ortho_discussions');
        $role->add_cap('edit_ortho_discussion');
        $role->add_cap('edit_ortho_discussions');
        $role->add_cap('edit_privet_ortho_discussions');
        $role->add_cap('publish_ortho_discussions');
        $role->add_cap('delete_others_ortho_discussions');
        $role->add_cap('delete_private_ortho_discussions');
        $role->add_cap('delete_published_ortho_discussions');
        $role->add_cap('delete_ortho_discussions');
        $role->add_cap('delete_ortho_discussion');

    }
}
add_action('admin_init','ortho_add_role_caps',999);