<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Template Name: Specialists
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();
get_header();
$fields = get_fields();
?>


<div class="hero hero-slider">
    <?php $slider  = $fields['slider']; 
    foreach($slider as $slide){ ?>
        <div class="slide" style="background-image:url(<?php echo $slide['image']['url']; ?>);">
            <div class="grid-container">
                <div class="grid-x">
                    <div class="large-12 cell">
                        <h1><?php echo $slide['title']; ?></h1>
                        <p><?php echo $slide['text']; ?></p>
                        <a class="button" href="<?php echo $slide['button_1']['url']; ?>"><?php echo $slide['button_1']['title']; ?> <span class="material-icons">keyboard_arrow_left</span></a>
                        <a class="link" href="<?php echo $slide['button_1']['url']; ?>"><?php echo $slide['button_1']['title']; ?></a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<div class="grid-container" id="filter">
    <div class="grid-x grid-margin-x">
        <div class="large-12 cell" >
        <?php  get_template_part('template-parts/filter'); ?>
        </div>
    </div>
</div>
<div class="grid-container m-50 text-row">
    <div class="grid-x grid-margin-x align-center" >
        <div class="large-6 cell text-center ">
            <h2><?php echo $fields['text']['title']; ?></h2>
            <p><?php echo $fields['text']['text']; ?></p>
        </div>
    </div>
</div>

<?php $forum_links =  $fields['forum_links']; ?>
<div class="grid-container m-50 wide forum-links" data-equalizer data-equalize-on="medium" data-equalize-by-row>
    <div class="grid-x grid-margin-x align-center" >
    <?php foreach($forum_links as $forum_link){ ?>
        <div class="large-auto small-12 cell text-center forum-links-item" >
            <img src="<?php echo $forum_link['icon']['url']; ?>" title="<?php echo $forum_link['icon']['title']; ?>" alt="<?php echo $forum_link['icon']['title']; ?>">
            <h5><?php echo $forum_link['title']; ?></h5>  
            <p data-equalizer-watch><?php echo $forum_link['text']; ?></p>
            <a class="button light" href="<?php echo $forum_link['link']; ?>"><?php _e('To the Forum' , 'podium'); ?> <i class="material-icons">add</i></a>
        </div>
    <?php } ?>
        
    </div>
</div>

<section class="articles bg-gray m-120">
<div class="grid-container ">
    <div class="grid-x grid-margin-x align-justify">
        <div class="shrink cell ">
        <h2><?php echo $fields['articles_title']; ?></h2>
        
        </div>
        <div class="shrink  cell ">
        <a href="<?php echo $fields['articles_link']['url']; ?>" class="articles-link"><?php echo $fields['articles_link']['title']; ?> <i class="material-icons">add</i></a>

        </div>
    </div>
</div>

<div class="grid-container m-20">
    <div class="grid-x grid-margin-x" data-equalizer data-equalize-on="medium" data-equalize-by-row>
<?php 
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => '4',
            'post__in' => $fields['articles'],
        );
    
        $wp_query = new wp_query( $args ); ?>
        <?php if ( have_posts() ) { ?>
            <?php while ( have_posts() ) { the_post(); ?> 
                <?php $term_list = get_the_terms( $post->ID, 'category' );  ?>
                        
                <div class="small-12 medium-6 large-3 cell single-post-feed" data-equalize-watch>
                    <a href="<?php the_permalink(); ?>">
                    <div class="image-wrap">
                        <span><?php echo $term_list[0]->name; ?></span>
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="text">
                        <h2><?php the_title(); ?></h2>
                        <p> <?php echo get_the_content_clean(10); ?> </p>
                    </div> 
                    </a>
                </div>
                
            <?php } ?>   
        <?php } // end if ?>
        <?php wp_reset_query(); ?>

        
    </div>
</div>
</section>

<section class="specialist-home">
<?php  $specialist_g =  $fields['specialist']; ?>
<div class="grid-container">
    <div class="grid-x">
        <div class="large-12 cell">
            <h2 class="text-center"> <?php echo $specialist_g['title']; ?> </h2>
        </div>
    </div>

   <div class="specialist-slider-wrap">
   <div class="specialist-slider">
   <?php 
        $args = array(
            'post_type' => 'specialist',
            'posts_per_page' => '10',
            // 'post__in' => $specialist_g['list'],
            'orderby'   => 'rand',
            'meta_key'		=> 'show_as_specialist',
	        'meta_value'	=> true

            
        );
        $wp_query = new wp_query( $args ); ?>
        <?php if ( have_posts() ) { ?>
            <?php while ( have_posts() ) { the_post(); ?>       
                <div class="spe-single-slider">
                    <a href="<?php the_permalink( ); ?>">
                    <?php if(has_post_thumbnail()){
                            the_post_thumbnail('thumbnail');
                        }else{
                            echo '<img src="' . get_template_directory_uri().'/dist/images/avatar.png">';
                        }
                    ?>
                    <h5><?php the_title(); ?> </h5>
                    <p> <?php  echo get_the_content_clean(14); ?></p>
                    </a>
                </div>
            <?php } ?>   
        <?php } // end if ?>
        <?php wp_reset_query(); ?>
   </div> 
   
    <div class="arrow-prev-spe"><i class="material-icons">keyboard_arrow_left</i></div>
    <div class="arrow-next-spe"><i class="material-icons">keyboard_arrow_right</i></div>
   </div>

   <div class="grid-x align-center m-40">
        <div class="shrink cell">
            <a class="button" href="<?php echo $specialist_g['button_1']['url']; ?>"> <?php echo $specialist_g['button_1']['title']; ?><i class="material-icons">add</i></a>
            <a class="link" href="<?php echo $specialist_g['button_2']['url']; ?>"> <?php echo $specialist_g['button_2']['title']; ?><i class="material-icons">search</i></a>
           
        </div>
    </div>
</div>
</section>


<section>
<div class="grid-container m-60">
    <div class="grid-x grid-margin-x align-justify" >
       
        <div class="large-8 cell">
            <?php $about_text = $fields['about_text']; ?>
            <div class="about-text">
                <h4><?php echo  $about_text['title']; ?></h4>
                <p><?php echo  $about_text['text']; ?></p>
                <a href="<?php echo  $about_text['link']['url']; ?>"><?php echo  $about_text['link']['title']; ?></a>
            </div>
            <?php $goals = $fields['goals']; ?>
            <div class="goals">
                <h4><span><?php echo  $goals['title']; ?></span></h4>
                <ul>
                    <?php foreach($goals['goals'] as $goal){
                        echo '<li><span>'. $goal['text'].'</span></li>';
                    }?>
                </ul>
            </div>
        </div>
        <div class="large-4 cell " >
            <?php $news = $fields['news']; ?>
            <div class="news" >
                <div class="top-news">
                <h4><?php echo  $news['title']; ?></h4>
                </div>
                
                <div class="news-slider">
                    <?php foreach($news['news'] as $new){ ?>
                        <div>
                            
                            <h6><?php echo $new['title'] ; ?></h6>
                            <p><?php echo $new['text'] ; ?></p>
                            <a href="<?php echo $new['link'];?>"><?php _e('Read more', 'podium'); ?> <span class="material-icons">keyboard_arrow_left</span> </a>
                                                  
                        </div>
                        
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>

</section>

<section class="bg-gray m-80 p-100" id="hp-counter">
    <?php $numbers = $fields['numbers']; ?>
    <div class="grid-container numbers">
        <div class="grid-x text-center">
            <?php foreach($numbers as $number){ ?>
            <div class="large-3 medium-6 small-12 cell">
                <?php if($number['is_symbol']== 'before'){ ?>
                    <?php echo '<i>' . $number['symbol'] . '</i>'; ?>
                <?php } ?>
                <span class="number" data-stop="<?php echo $number['number']; ?>"><?php echo $number['number']; ?></span>
                <?php if($number['is_symbol']== 'after'){ ?>
                    <?php echo '<i>' . $number['symbol'] . '</i>'; ?>
                <?php } ?>

                <p><?php echo $number['text']; ?></p>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
 
<?php get_footer();
