<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package podium
 */

?>
<footer class="site-footer">
   <div class="grid-container">
      <div class="grid-x grid-margin-x">
         <div class="large-3 small-12 medium-6 cell footer-column"> 
            <?php  $column_1 = (get_field('column_1','option_footer_' . ICL_LANGUAGE_CODE)) ? get_field('column_1','option_footer_' . ICL_LANGUAGE_CODE) : get_field('column_1','option_footer_all'); ?> 
            <ul class="footer-menu">
            <h5> <?php echo $column_1['title']; ?></h5> 
               <?php
                  $args = array(
                     'post_type' => 'page',
                     'posts_per_page' => '6',
                     'post__in' => $column_1['links'],
                  );
               $wp_query = new wp_query( $args ); ?>
               <?php if ( have_posts() ) { ?> 
                  <?php while ( have_posts() ) { the_post(); ?>
                     <li><a href="<?php the_permalink(); ?>" > <?php the_title(); ?></a></li>
                  <?php } // end while ?>
               <?php } // end if ?>
               <?php wp_reset_query(); ?>
            </ul>
         </div>

         <div class="large-3 small-12 medium-6 cell footer-column"> 
            <?php  $column_2 = (get_field('column_2','option_footer_' . ICL_LANGUAGE_CODE)) ? get_field('column_2','option_footer_' . ICL_LANGUAGE_CODE) : get_field('column_2','option_footer_all'); ?> 
            
            <ul class="footer-menu">
            <h5> <?php echo $column_2['title']; ?></h5> 
            <?php
            foreach($column_2['links'] as $link){ ?>
           
               <li><a href="<?php echo $link['link']['url']; ?>" ><?php echo $link['link']['title']; ?> </a></li>
            <?php } ?>
            </ul>
                       
         </div>
        
         <div class="large-3 small-12 medium-6 cell footer-column"> 
            <?php  $column_3 = (get_field('column_3','option_footer_' . ICL_LANGUAGE_CODE)) ? get_field('column_3','option_footer_' . ICL_LANGUAGE_CODE) : get_field('column_3','option_footer_all'); ?> 
            <ul class="footer-menu">
            <h5> <?php echo $column_3['title']; ?></h5> 
               <?php
                  $args = array(
                     'post_type' => 'guide',
                     'posts_per_page' => '6',
                     'post__in' => $column_3['links'],
                  );
               $wp_query = new wp_query( $args ); ?>
               <?php if ( have_posts() ) { ?> 
                  <?php while ( have_posts() ) { the_post(); ?>
                     <li><a href="<?php the_permalink(); ?>" ><?php the_title(); ?>  </a></li>
                  <?php } // end while ?>
               <?php } // end if ?>
               <?php wp_reset_query(); ?>
             </ul>
         </div>

         <div class="large-3 small-12 medium-6 cell footer-column"> 
            <?php  $column_4 = (get_field('column_4','option_footer_' . ICL_LANGUAGE_CODE)) ? get_field('column_4','option_footer_' . ICL_LANGUAGE_CODE) : get_field('column_4','option_footer_all'); ?> 
            
            <ul class="footer-menu">
            <h5> <?php echo $column_4['title']; ?></h5> 
            <?php
            foreach($column_4['links'] as $link){ ?>
               <li><a href="<?php echo $link['link']['url']; ?>" ><?php echo $link['link']['title']; ?> </a></li>
            <?php } ?>
            </ul>
                       
         </div>
      </div>
   </div>   
<div class="credits">
<div class="grid-container">
 	<div class="grid-x grid-padding-x align-justify">
      <div class="small-12 cell">
         <div class="shrink cell">
            <?php _e('All rights reserved', 'podium'); ?> &copy; <?php bloginfo('name'); ?>
         </div>
         <div class="shrink cell">
            Website by  <a href="http://elilev.me/" rel="designer" target="_black">Weepo</a> | Design by <a href="#"  target="_black"> Bambalick </a>
         </div>
      </div>
</div> 	
</div> 	
</footer>
</div><!-- .grid-container -->
</div><!-- .off-canvas-content -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
