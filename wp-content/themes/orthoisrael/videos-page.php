<?php
    /**
    * Template Name: Videos 
     *
     * @package podium
     */

    get_header();

    $height = 'tall';
    include( locate_template( 'template-parts/page-top.php', false, false ) );
   
   
?>
<div class="grid-container">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area  small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        <div class="large-12 cell">
        <?php 
        $args = array(
            'post_type' => 'video',
            'posts_per_page' => '-1',
            'orderby' => 'slug',
            'order'    =>'ASC'
        ); 
        $wp_query = new wp_query( $args ); ?>
        <?php if (have_posts()) { ?>
            <div class="grid-x grid-margin-x video-feed m-60">
            <?php while (have_posts()) { the_post(); ?>
                <?php $term_list = get_the_terms( $post->ID, 'category' );  ?>
                        
                <div class="small-12 medium-6 large-3 cell single-post-feed">
                    <a href="<?php the_permalink(); ?>">
                    <div class="image-wrap">
                        <!-- <span><?php //echo $term_list[0]->name; ?></span> -->
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="text">
                        <h2><?php the_title(); ?></h2>
                        <p> <?php echo get_the_content_clean(10); ?> </p>
                    </div> 
                    </a>
                </div>
            <?php } ?>
            </div>
        <?php  } ?>
           
        </div>
    </div>           
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();