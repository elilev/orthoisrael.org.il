<?php
/**
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();

get_header();
$height = 'tall';
include( locate_template( 'template-parts/page-top.php', false, false ) );
?>

<div class="grid-container">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area offset small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        <div class="large-4 cell side-bar">
            <?php  get_template_part('template-parts/sidebar-menu'); ?>    
        </div>
        <div class="large-8 cell">
        <div class="content-block"> 
        <?php

          while (have_posts()) { the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
              <div class="entry-content">
                <?php the_content(); ?>
              </div><!-- .entry-content -->
            </article><!-- #post-## -->
        <?php } ?>
        </div>
        </div>
    </div>           
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();



