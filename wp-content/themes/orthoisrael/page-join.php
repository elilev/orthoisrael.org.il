<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Template Name: Join
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();
get_header();
$height = 'tall';
include( locate_template( 'template-parts/page-top.php', false, false ) );
$fields = get_fields();
?>
<div class="grid-container join-page">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area offset small-12 large-12 cell align-center">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x  align-center">
        <div class="large-8 cell m-40">
            <div class="text-center">
                <h2><?php echo $fields['title']; ?> </h2>
                <p><?php echo $fields['text']; ?> </p>
            </div>
            <div class="form-wrap box">
                <?php echo do_shortcode( $fields['form'] ); ?>
            </div>
           
        </div>
    </div>
             
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();
