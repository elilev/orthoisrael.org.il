<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Template Name: Specialists
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();
get_header();
$height = 'short';
include( locate_template( 'template-parts/page-top.php', false, false ) );
?>
<div class="grid-container">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area  small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
       
        <div class="large-12 cell">
            <?php  get_template_part('template-parts/filter'); ?>
        </div>

       

        <div class="large-12 cell m-40">
        <?php
        $args = array(
            'post_type' => 'specialist',
            'posts_per_page' => '-1',
            'orderby' => 'slug',
            'order'    =>'ASC',
            'post_status' => 'publish'
            
        );
        if(isset($_GET['area']) && $_GET['area']!= '' && $_GET['city'] == 'all'){
            $args['tax_query'][] = array(
                array(
                    'taxonomy' => 'location',
                    'field'    => 'term_id',
                    'terms'    =>  $_GET['area'],
                ),
            );             
        }
        //overwrite location if city is set to Something Different then "all"
        if(isset($_GET['city']) && $_GET['city']!= '' && $_GET['city'] != 'all'){
            $args['tax_query'][] = array(
                array(
                    'taxonomy' => 'location',
                    'field'    => 'term_id',
                    'terms'    =>  $_GET['city'],
                ),
            );             
        }
        if($_GET['city'] == 'all'  &&  $_GET['area'] == 'all'){
            $args['tax_query'] = '';  
        }
        if(isset($_GET['sn']) && $_GET['sn']!= ''){
            $args['p'] = $_GET['sn'];
        }
       
            
    
        $wp_query = new wp_query( $args ); ?>
        <div class="grid-x grid-margin-x specialist-list filter-results" data-equalizer data-equalize-on="medium" data-equalize-by-row>  
        <?php if ( have_posts() ) { ?>
                <?php while ( have_posts() ) { the_post(); ?>
                    <?php  get_template_part('template-parts/card'); ?>
                <?php } // end while ?>   
        <?php } else{
            _e('No results','podium'); 
        }// end if ?>
        </div>
        <?php wp_reset_query(); ?>
        </div>
    </div>
             
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();
