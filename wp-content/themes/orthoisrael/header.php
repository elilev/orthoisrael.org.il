<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="//gmpg.org/xfn/11">

    <?php
    wp_head();
    get_template_part('template-parts/layouts/favicon', '');
    ?>
<?php if (!isset($_GET['skip-google'])) : ?>    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-CZMWVWGVFB"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-CZMWVWGVFB');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '298906071743907'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
    <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=298906071743907&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Ads: 872792061 -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-872792061"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'AW-872792061');
    </script>

    <meta name="facebook-domain-verification" content="siiaq8nen4qo054rv3ytjds6n5i8rp" />
<?php endif; ?>
</head>

<body <?php body_class(); ?>>

    <div id="page" class="hfeed site">
        <a class="skip-link show-for-sr" href="#content">
            <?php esc_html_e('Skip to content', 'podium'); ?>
        </a>

        <header id="masthead" class="site-header" role="banner" data-sticky-container>
            <div class="show-for-medium top-bar-wrap grid-x align-justify" data-sticky data-margin-top="0">  
            <?php  $social = (get_field('socail','option_general_' . ICL_LANGUAGE_CODE)) ? get_field('socail','option_general_' . ICL_LANGUAGE_CODE) : get_field('socail','option_general_all'); ?> 
                <div class="large-12 cell top-nav">
                    <form class="search-form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <input type="search" class="search-field" placeholder="חיפוש …" value="" name="s">
                        <span class="material-icons search-icon">search</span>
                        <input type="submit" class="search-submit" value="">
                    </form>
                    <?php
                            if (function_exists('pll_the_languages')) { 
                                $translations = pll_the_languages(array('raw'=>1));
                                if($translations['he']['current_lang']){ ?>
                                    <a href="<?php echo $translations['en']['url']; ?>" class="lang social-link"><i>EN</i> </a> 
                                <?php
                                    }elseif ($translations['en']['current_lang']) { ?>
                                    <a href="<?php echo $translations['he']['url']; ?>" class="lang social-link"><i> עב</i></a>
                                <?php

                            }
                        }
                        ?> 
                    <a class="social-link" href="mailto:<?php echo  $social['email']; ?>"><i class="material-icons">email</i></a>
                    <a class="social-link" href="<?php echo  $social['facebook']; ?>"><i class="fab fa-facebook-f"></i></a>
                    <a class="social-link" href="<?php echo  $social['instagram']; ?>"> <i class="fab fa-instagram"></i></a>   
                </div>
                <div class="shrink cell">
                <a href="<?php echo get_home_url(); ?>" class="logo">
                    <span class="title-bar-title"><?php echo get_bloginfo('name'); ?></span>
                </a>
                </div>
                <div class="top-nav shrink cell">
                    <?php $settings->getMenu(new Top_Bar_Walker(), 'onCanvass'); // print menu (source config.php) ?>
                    
                </div>
                <div class="shrink cell">
                  
                    <?php  $asso_button = (get_field('asso_button','option_general_' . ICL_LANGUAGE_CODE)) ? get_field('asso_button','option_general_' . ICL_LANGUAGE_CODE) : get_field('asso_button','option_general_all'); ?>
                    <button class="button asso-button" type="button" data-toggle="asso-dropdown" >
                        <span class="material-icons">assignment_ind</span>
                        <?php echo $asso_button['main_button']; ?>
                    </button>
    
                    <div class="dropdown-pane" id="asso-dropdown" data-dropdown data-auto-focus="true"  data-hover="true" data-hover-pane="true" data-position="bottom" data-alignment="center">
                        <a class="button dark" href="<?php echo $asso_button['join_button']['url']; ?>">
                            <span class="material-icons">person_add</span>
                            <?php echo $asso_button['join_button']['title']; ?> 
                        </a>
                        <a class="link" href="<?php echo $asso_button['link']['url']; ?>">
                            <?php echo $asso_button['link']['title']; ?> 
                        </a>
                    </div>
                </div>
               

               
                
            </div>

            <?php if(wp_is_mobile(  )){ ?>
            <div class="hide-for-medium top-mobile-header">
                <div class="mobile-bar">
                    <div class="mobile-bar-left mobile-bar-logo" >
                        <a href="<?php echo get_home_url(); ?>">
                            <span class="mobile-bar-title"><?php echo get_bloginfo('name'); ?></span>
                        </a>
                    </div>

                    <div class="mobile-bar-right mobile-bar-menu">
                        <button class="menu-icon" type="button" data-open="offCanvas"></button>
                    </div>
                </div>
            </div>
            <?php } ?>
        </header><!-- #masthead -->
        <?php if(wp_is_mobile(  )){ ?>       
        <div class="off-canvas position-left" id="offCanvas" data-off-canvas>

            <!-- Close button -->
            <button class="close-button" aria-label="Close menu" type="button" data-close>
                <span aria-hidden="true">&times;</span>
            </button>

            <!-- Menu -->
            <?php $settings->getMenu(new Top_Bar_Walker(), 'offCanvas'); ?>
            <div class="text-center">
                <a class="button dark" href="<?php echo $asso_button['join_button']['url']; ?>">
                    <span class="material-icons">person_add</span>
                    <?php echo $asso_button['join_button']['title']; ?> 
                </a>
                <a class="link" href="<?php echo $asso_button['link']['url']; ?>">
                    <?php echo $asso_button['link']['title']; ?> 
                </a>
            </div>
        </div>
        <?php } ?>
        <div class="off-canvas-content" data-off-canvas-content>

        