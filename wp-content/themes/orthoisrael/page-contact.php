<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Template Name: Contact
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();
get_header();
$height = 'tall';
include( locate_template( 'template-parts/page-top.php', false, false ) );
$fields = get_fields();
?>
<div class="grid-container contact-page">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area offset small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        <div class="large-8 cell m-40">
            <div class="form-wrap box">
                <h2><?php echo $fields['title']; ?> </h2>
                <p><?php echo $fields['sub_title']; ?> </p>
                <?php echo do_shortcode( $fields['form'] ); ?>
            </div>
           
        </div>
        <?php  $social = (get_field('socail','option_general_' . ICL_LANGUAGE_CODE)) ? get_field('socail','option_general_' . ICL_LANGUAGE_CODE) : get_field('socail','option_general_all'); ?> 
        <div class="large-4 cell m-40">
            <div class="details box">
                <!-- <p>
                    <i class="material-icons">call</i>
                    <span><?php _e('Call us','podium'); ?></span>
                    <a href="tel:<?php echo $fields['phone']; ?>"><?php echo $fields['phone']; ?></a>
                </p> -->
                <!-- <p>
                    <i class="material-icons">location_on</i>
                    <span><?php _e('Our address','podium'); ?></span>
                    <?php echo $fields['address']; ?>
                </p> -->
                <p>
                  
                    <?php echo $fields['text_area']; ?><br/>
                    <a class="social-link" href="<?php echo  $social['facebook']; ?>"><i class="fab fa-facebook-f"></i></a>
                    <a class="social-link" href="<?php echo  $social['instagram']; ?>"> <i class="fab fa-instagram"></i></a>   
                </p>
                
                
            </div>
      
           
        </div>
    </div>
             
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();
