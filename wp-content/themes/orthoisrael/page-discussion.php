<?php
    /**
    * Template Name: Discussion
     *
     * @package podium
     */

    get_header();


    $height = 'tall';
    include( locate_template( 'template-parts/page-top.php', false, false ) );
   
   
?>

<div class="grid-container">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area offset small-12 large-12 cell">

<main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        <div class="large-4 cell side-bar">
            <?php
            $category_menu = true;
            $category_name = 'discussion-type';
            include( locate_template( 'template-parts/sidebar-menu.php', false, false ) ); ?>    
        </div>
        <div class="large-8 cell discussion-archive">

        <div class="grid-x grid-padding-x m-40  px-2">
            <div class="small-12 medium-12 cell">
                
                <div class="entry-content">
                
                </div>
            </div>
        </div>

        <?php $terms = get_terms( 'discussion-type', array(
                'hide_empty' => false,
            ) );

            foreach($terms as $term){
                $args = array(
                    'post_type' => 'discussion',
                    'posts_per_page' => '4',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'discussion-type',
                            'field'    => 'id',
                            'terms'    => $term->term_id,
                        ),
                    ),  
                );
                $discussion_query = new wp_query( $args );
        
                    ?>
                    <div class="grid-x grid-padding-x m-40  px-2">
                        <div class="small-12 medium-12 cell">
                            
                            <div class="entry-content">
                            <h3><?php echo $term->name; ?></h3>
                                <p><?php echo $term->description; ?><p>
                            </div>
                        </div>
                    </div>

                    
                    <?php if ($discussion_query->have_posts()) { ?>
                        <div class="grid-x grid-margin-x  m-20 discussion-list px-2">
                        <?php while ($discussion_query->have_posts()) { $discussion_query->the_post(); ?>
                            <div class="small-12 cell discussion-list-single">
                            <div>    
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                <?php if( get_field('name') ){ ?>
                                    <div><span><?php _e('Posted by:','podium'); ?> <?php  echo the_field('name'); ?></span></div>
                                <?php } ?>
                                <div><span> <?php echo get_the_date('d.m.Y'); ?> </span></div>
                            </div>
                            <div class="comments-num">
                                    <i class="material-icons">speaker_notes</i><?php echo get_comments_number(); ?>
                            </div>
                            </div>
                        <?php } ?>
                        </div>
                    <?php  } 
                } ?>

        </div>
    </div>

   
                       
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();
