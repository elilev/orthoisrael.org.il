<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package podium
 */

/*
    * If the current post is protected by a password and
    * the visitor has not yet entered the password we will
    * return early without loading the comments.
    */

if (post_password_required()) {
    return;
}

?>

<div id="comments" class="comments-area py-2">
    <?php // You can start editing here -- including this comment! ?>
    <?php
        if (have_comments() && (comments_open() || get_comments_number())) {
        ?>
        <!-- <h2 class="comments-title">
            <?php
                // WPCS: XSS OK.
                    printf(
                        esc_html(
                            _nx(
                                'One thought on &ldquo;%2$s&rdquo;',
                                '%1$s thoughts on &ldquo;%2$s&rdquo;',
                                get_comments_number(),
                                'comments title',
                                'podium'
                            )
                        ),
                        number_format_i18n(get_comments_number()),
                        '<span>' . get_the_title() . '</span>'
                    );
                ?>
        </h2> -->
        <?php

                if (get_comment_pages_count() > 1
                && get_option('page_comments')) { // Are there comments to navigate through? ?>
                <nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
                    <h2 class="screen-reader-text"><?php esc_html_e('Comment navigation', 'podium');?></h2>
                    <div class="nav-links">
                        <div class="nav-previous"><?php previous_comments_link(
                                                          esc_html__('Older Comments', 'podium')
                                                      );?></div>
                            <div
                            class="nav-next"><?php next_comments_link(esc_html__('Newer Comments', 'podium'));?></div>

                        </div><!-- .nav-links -->
                    </nav><!-- #comment-nav-above -->
                <?php }

                    // Check for comment navigation. ?>

   <ul class="comment-list">
        <?php $args = array(
                'style'             => 'ul',
                'callback'          => 'podium_comment',
                'format'            => 'html5',
                'reply_text'        => 'להגיב',
                'short_ping'        => true,
                'echo'              => true
            ); ?>
    <?php wp_list_comments( $args ); ?>

    </ul><!-- .comment-list -->

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) {?>
        <nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
            <h2 class="screen-reader-text"><?php esc_html_e('Comment navigation', 'podium');?></h2>
            <div class="nav-links">
                <div class="nav-previous"><?php previous_comments_link(esc_html__('Older Comments', 'podium'));?></div>
                <div class="nav-next"><?php next_comments_link(esc_html__('Newer Comments', 'podium'));?></div>
            </div><!-- .nav-links -->
        </nav><!-- #comment-nav-below -->
    <?php }?>
<?php }?>

<?php if (!comments_open()
        && '0' != get_comments_number()
        && post_type_supports(get_post_type(), 'comments')) {
    ?>
    <p class="no-comments"><?php esc_html_e('Comments are closed.', 'podium');?></p>
<?php }?>
<div id="respond" class="comment-form">
    <div class="grid-x grid-padding-x">
        <div class="auto cell"><h2>כתוב תגובה:</h2></div>
        <div class="shrink cell">
            <div class="cancel-comment-reply">
					<?php cancel_comment_reply_link(); ?>
			</div>
        </div>
    </div>    
    <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform" data-abide novalidate>
            <div data-abide-error class="alert callout" style="display: none;">
                <p><i class="fi-alert"></i> <?php _e('Please fill in the required fields','podium'); ?></p>
            </div>
            
            <div class="grid-x grid-padding-x">
                <div class="large-6 cell">
                    <label for="author"><?php _e('Name','podium'); ?></label>
                    <input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" tabindex="2" required />
                    <span class="form-error">
                    <?php _e('Your name is required', 'podium'); ?>
                    </span>
                    <?php if(is_user_logged_in()){ ?>
                        <p> איזור זה נראה למנהלים בלבד, יש לבחור מהרשימה את שם המגיב </p>
                        <?php  $list = (get_field('specialist_list','option_general_' . ICL_LANGUAGE_CODE)) ? get_field('specialist_list','option_general_' . ICL_LANGUAGE_CODE) : get_field('specialist_list','option_general_all'); ?> 
                        <select class="specialist-list-select">
                        <option>בחר מומחה מהרשימה</option>
                            <?php foreach($list as $list_item){ 
                                $specialist_id = $list_item['specialist'];
                                echo '<option data-email="' .get_field('email',$specialist_id) . '">' . get_the_title($specialist_id) . '</option>';
                            }
                            ?>
                        </select>
                    <?php } ?>
                </div>

                <div class="large-6 cell">
                    <label for="email"><?php _e('Email address', 'podium'); ?></label>
                    <input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>"  tabindex="3" required/>
                    <span class="form-error">
                    <?php _e('Please fill your Email address', 'podium'); ?>
                    </span>
                </div>
            </div>

            <div>
                <label><?php _e('Your comment','podium'); ?>  </label>
                <textarea name="comment" id="comment" rows="4" tabindex="1" required></textarea>
                <span class="form-error">
                <?php _e('Please add content to your comment', 'podium'); ?>
                </span>

            </div>


            <div>
                <button class="button" type="submit" value="Submit"><?php _e('Submit','podium'); ?></button>
                <?php comment_id_fields(); ?>
            </div>

        <?php do_action('comment_form', $post->ID); ?>

        <?php //comment_form();?>

    </form>
	</div>

</div><!-- #comments -->


