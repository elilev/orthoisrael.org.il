<?php
/**
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();

get_header();
$height = 'tall';
include( locate_template( 'template-parts/page-top.php', false, false ) );
?>
<div class="grid-container">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area offset small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        <div class="large-4 cell side-bar">
            <?php
            $menu_name = 'info';
            include( locate_template( 'template-parts/sidebar-menu.php', false, false ) ); ?>    
        </div>
        <div class="large-8 cell">
        <?php

            while (have_posts()) {the_post();
                get_template_part('template-parts/content-single-post');
            }
        ?>

        </div>
    </div>

   
                       
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();