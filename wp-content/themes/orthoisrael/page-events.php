<?php
/**
 *
 * Template Name: Events
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();
get_header();
$height = 'tall';
include( locate_template( 'template-parts/page-top.php', false, false ) );
?>
<div class="grid-container">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area offset small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        <div class="large-12 cell">
        <?php
        $args = array(
            'post_type' => 'event',
            'posts_per_page' => '-1',
        );
    
        $wp_query = new wp_query( $args ); ?>
        <?php if ( have_posts() ) { ?>
            <div class="events-list content-block">  
                <?php while ( have_posts() ) { the_post(); ?>
                <article class="large-12 cell events-list-single mb-20">
                    <div class="grid-x grid-padding-x">
                        <div class="large-3 cell">
                            <?php the_post_thumbnail('square-image'); ?>
                        </div>
                        <div class="large-9 cell events-list-single-content">
                            
                            <h3><?php the_title(); ?> </h3>
                            <p><?php  echo get_the_content_clean(28); ?></p>


                            <?php 
                                $date = get_field('date');
                                $time = get_field('time');
                                $location =get_field('location');
                                $event_link = get_field('event_link');
                            ?>
                            <div class="grid-x align-middle">
                                <div class="large-8 cell">
                                <?php
                                    if($date){
                                        echo '<div class="events--info"><i class="material-icons">event</i>';
                                        echo '<time>' . $date , '</time></div>';
                                    }
                                    if($time){
                                        echo '<div class="events--info"><i class="material-icons">watch_later</i>';
                                        echo '<time>' . $time , '</time></div>';
                                    }
                                    if($location){
                                        echo '<div class="events--info"><i class="material-icons">place</i>';
                                        echo '<span>' . $location , '</span></div>';
                                    }
                                ?>
                                </div>
                                <div class="large-4 cell">
                                    <?php
                                        if($event_link){
                                            echo '<div class="text-left"><a class="button" href="'.$event_link['url'].'" target="_blank">'. $event_link['title'].'</a></div<';
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <?php } // end while ?>
            </div>
        <?php } // end if ?>
        <?php wp_reset_query(); ?>

        </div>

       

    </div>
    
    
               
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();
