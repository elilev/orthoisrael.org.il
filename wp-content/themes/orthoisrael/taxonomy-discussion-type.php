<?php
    /**
     * The main template file.
     *
     * This is the most generic template file in a WordPress theme
     * and one of the two required files for a theme (the other being style.css).
     * It is used to display a page when nothing more specific matches a query.
     * E.g., it puts together the home page when no home.php file exists.
     * Learn more: http://codex.wordpress.org/Template_Hierarchy
     *
     * @package podium
     */

    get_header();

    $queried_object = get_queried_object();

    $term_id = $queried_object->term_id; 
    $height = 'tall';
    $pagetitle = $queried_object->name;
    include( locate_template( 'template-parts/page-top.php', false, false ) );
   
   
?>
    <script src="https://www.google.com/recaptcha/api.js?render=6LclMCQbAAAAAFPrwFR6ujR1xJyWLBJq0aUVr4SG"></script>

<div class="grid-container">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area offset small-12 large-12 cell">

<main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        <div class="large-4 cell side-bar">
            <?php
            $category_menu = true;
            $category_name = 'discussion-type';
            include( locate_template( 'template-parts/sidebar-menu.php', false, false ) ); ?>    
        </div>
        <div class="large-8 cell discussion-archive ">
        <div class="grid-x grid-padding-x ">
            <div class="small-12 medium-12 cell">

            
                <?php if(isset($_GET['res'])){ ?>
                    <?php if($_GET['res'] == 1) { ?>
                        <div class="success callout">
                            <?php _e('Your Post was successfully published','podium'); ?>
                            <button class="close-button">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                <?php 
                    }elseif($_GET['res'] == 2){ ?>
                    <div class="alert callout">
                            <?php _e('An error has occurred','podium'); ?>
                            <button class="close-button">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>

                    <?php }
                }
                ?>
                
                <div class="entry-content m-60">
                    <?php echo category_description(); ?>
                    <?php  $specialists = (get_field('specialist_list','option_general_' . ICL_LANGUAGE_CODE)) ? get_field('specialist_list','option_general_' . ICL_LANGUAGE_CODE) : get_field('specialist_list','option_general_all'); 
                    $specialists_email_list = array(); ?>
                    <div class="grid-x grid-padding-x">
                        <?php foreach ($specialists as $specialist){ 
                   
                                $sid = $specialist['specialist'];
                                if (is_array($specialist['forum']) && in_array($term_id, $specialist['forum'])) { ?>
                                <div class="large-4 cell text-center">
                                    <?php
                                    if(get_field('email', $sid)){
                                        array_push($specialists_email_list , get_field('email', $sid));
                                    }
                                    ?>
                                    <div class="specialist-forum grid-x m-20 ">
                                        <div class="medium-12 cell mb-20">
                                            <div class="px-2">
                                            <?php if(has_post_thumbnail($sid)){
                                                        echo get_the_post_thumbnail($sid,array( 100, 100)); 
                                                    }else{
                                                        echo '<img src="' . get_template_directory_uri().'/dist/images/avatar.png">';
                                                    }
                                            ?>
                                            </div>
                                        </div>
                                        <div class="medium-12 cell ">
                                            <h3> <?php echo get_the_title($sid); ?></h3>
                                            <p> <?php echo strip_tags(get_field('short_text', $sid)); ?> </p>
                                        </div>
                                    </div>
                                </div>
                                <?php    
                                }  
                                ?>
                               
                           
                        <?php  }?>
                    </div>
                    
                </div>
                <div class="text-center m-20 mb-20">
                    <a href="#" id="new-topic-button" class="newtopic button"><?php _e('New Topic', 'podium'); ?></a>
                </div>
                <form  action="<?php echo get_template_directory_uri() ?>/form-process.php" method="post" class="new-topic-form" id="new-topic-form" data-abide novalidate>
                    <div data-abide-error class="alert callout" style="display: none;">
                        <p><i class="fi-alert"></i> <?php _e('Please fill in the required fields','podium'); ?></p>
                    </div> 
                    <div class="grid-x grid-padding-x">
                        <div class="small-12 cell">
                            <label for="topic"><?php _e('Topic','podium'); ?></label>
                            <input type="text" name="topic" id="topic"   required  >
                            <span class="form-error"><?php _e('Topic is required', 'podium'); ?></span>
                        </div>

                        <div class="small-12 cell">
                            <label for="content"><?php _e('Content','podium'); ?>  </label>
                            <textarea name="content" id="content" rows="3"  required></textarea>
                            <span class="form-error"><?php _e('Please add content', 'podium'); ?></span>
                        </div>
                        <div class="small-12 cell">
                            <label for="author"><?php _e('Name','podium'); ?></label>
                            <input type="text" name="author" id="author"   required  >
                            <span class="form-error"><?php _e('Your name is required', 'podium'); ?></span>
                        </div>
                        <div class="small-12 cell">
                            <label for="email"><?php _e('Email address', 'podium'); ?></label>
                            <input type="text" name="email" id="email"    required >
                            <span class="form-error"><?php _e('Please fill your Email address', 'podium'); ?></span>
                        </div>
                        <input type="hidden" name="submit-new-topic">
                        <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
                        <input type="hidden" name="term" value="<?php echo  $term_id; ?>">
                        <?php 
                            foreach($specialists_email_list as $item){
                                echo '<input type="hidden" name="recipients[]" value="'. $item. '">';
                            }
                        ?>
                        <div class="small-12 cell">
                            <button class="button" type="submit"  value="Submit" ><?php _e('Submit','podium'); ?></button>
                            <input type="reset" value="<?php _e('Reset', 'podium'); ?>" class="reset-form">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    
        <?php 
   
        
       
         if (have_posts()) { ?>
            <div class="grid-x grid-margin-x  m-60 discussion-list px-2">
            <?php while (have_posts()) { the_post(); ?>
                <?php $term_list = get_the_terms( $post->ID, 'category' );  ?>
                        
                <div class="small-12 cell discussion-list-single">
                <div>    
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    <?php if( get_field('name') ){ ?>
                        <div><span><?php _e('Posted by:','podium'); ?> <?php  echo the_field('name'); ?></span></div>
                    <?php } ?>
                    <div><span> <?php echo get_the_date('d.m.Y'); ?> </span></div>
                </div>
                <div class="comments-num">
                        <i class="material-icons">speaker_notes</i><?php echo get_comments_number(); ?>
                </div>
                </div>
            <?php } 
            ?>
            </div>
            <div class="row">
                <div class="grid-x grid-margin-x align-center text-center">
                    <div class="large-10 cell"><?php   podium_pagination(); ?></div>
                </div>
            </div>

        <?php  } ?>
  
        </div>
    </div>

   
                       
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();
