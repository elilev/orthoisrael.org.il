<?php
    /**
     * The main template file.
     *
     * This is the most generic template file in a WordPress theme
     * and one of the two required files for a theme (the other being style.css).
     * It is used to display a page when nothing more specific matches a query.
     * E.g., it puts together the home page when no home.php file exists.
     * Learn more: http://codex.wordpress.org/Template_Hierarchy
     *
     * @package podium
     */
    use Podium\Config\Settings as settings;

    $settings = new settings();

    get_header();
    $height = 'short';
    include( locate_template( 'template-parts/page-top.php', false, false ) );
    $fields = get_fields();
?>

<div class="grid-container">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area  small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        <div class="large-12 cell">
       
        <?php if (have_posts()) { ?>
            <div class="grid-x grid-margin-x news-feed">
            <?php while (have_posts()) { the_post(); ?>
                <?php $term_list = get_the_terms( $post->ID, 'category' );  ?>
                        
                <div class="small-12 medium-6 large-3 cell single-post-feed">
                    <a href="<?php the_permalink(); ?>">
                    <div class="image-wrap">
                        <span><?php echo $term_list[0]->name; ?></span>
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="text">
                        <h2><?php the_title(); ?></h2>
                        <p> <?php echo get_the_content_clean(10); ?> </p>
                    </div> 
                    </a>
                </div>
            <?php } ?>
            </div>
        <?php  } ?>
           
        </div>
    </div>           
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();
