<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * Template Name: Guide
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();
get_header();
$height = 'tall';
include( locate_template( 'template-parts/page-top.php', false, false ) );
?>
<div class="grid-container">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area offset small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        <div class="large-4 cell side-bar">
       
            <?php  get_template_part('template-parts/sidebar-menu'); ?>    
        </div>
        <div class="large-8 cell">
        <?php
        $args = array(
            'post_type' => 'guide',
            'posts_per_page' => '-1',
        );
    
        $wp_query = new wp_query( $args ); ?>
        <?php if ( have_posts() ) { ?>
            <div class="guide-list content-block">  
                <?php while ( have_posts() ) { the_post(); ?>
                <article class="large-12 cell guide-list-single mb-20">
                    <div class="grid-x grid-padding-x">
                        <div class="large-4 cell">
                            <a href="<?php the_permalink(); ?>" >
                                <div class="image-wrap">
                                    <?php the_post_thumbnail('square-image'); ?>
                                </div>
                            </a>
                        </div>
                        <div class="large-8 cell guide-list-single-content">
                            <a href="<?php the_permalink(); ?>" >
                                <h3><?php the_title(); ?> </h3>
                                <p>
                                <?php 
                                    echo get_the_content_clean(28);
                                ?>
                                </p>
                                <time> <?php echo get_the_date('d/m/Y'); ?> </time> 
                            </a>
                        </div>
                    </div>
                </article>
                <?php } // end while ?>
            </div>
        <?php } // end if ?>
        <?php wp_reset_query(); ?>

        </div>

       

    </div>
    
    
               
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();
