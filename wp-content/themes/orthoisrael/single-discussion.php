<?php
/**
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();

get_header();
$height = 'tall';
$terms = get_the_terms(get_the_ID(), 'discussion-type');
$pagetitle = $terms[0]->name;
include( locate_template( 'template-parts/page-top.php', false, false ) );
?>
<div class="grid-container">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area offset small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        
        <div class="large-4 cell side-bar">
            <?php
             $category_menu = true;
             $category_name = 'discussion-type';
            include( locate_template( 'template-parts/sidebar-menu.php', false, false ) ); ?>    
        </div>
        <div class="large-8 cell">
        <?php if(isset($_GET['res'])){ ?>
                    <?php if($_GET['res'] == 1) { ?>
                        <div class="success callout m-40">
                            <?php _e('Your Post was successfully published','podium'); ?>
                            <button class="close-button">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php }
                }
        ?>
        <?php

            while (have_posts()) {the_post();
                get_template_part('template-parts/content-single-discussion');
            }
        ?>

        <div class="grid-x grid-padding-x">
            <div class="large-12 cell">
            <?php  
                if ( comments_open() || get_comments_number() ) {
                    comments_template();
                }
            ?>
            </div>
        </div>

        </div>
    </div>



   

   
                       
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();