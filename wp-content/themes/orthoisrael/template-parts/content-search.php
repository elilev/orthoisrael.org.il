<article class="large-12 cell  mb-20 single-search-item">
<div class="grid-x grid-padding-x">
	<div class="large-4 cell">
		<a href="<?php the_permalink(); ?>" >
			<div class="image-wrap">
				<?php the_post_thumbnail('square-image'); ?>
			</div>
		</a>
	</div>
	<div class="large-8 cell">
		<a href="<?php the_permalink(); ?>" >
			<h3><?php the_title(); ?> </h3>
			<p>
			<?php 
				echo get_the_content_clean(28);
			?>
			</p>
		</a>
	</div>
</div>
</article>