<?php
if ( !isset($category_menu) ) $category_menu = false;
if(!$category_menu){
    if ( !isset($menu_name) ) $menu_name = get_field( 'show_menu' );


   
    if($menu_name == 'info'){
        $menu_name = 'info-nav';
    }else{
        $menu_name = 'about-nav';
    }
   
    $locations = get_nav_menu_locations();
    $current_page = get_the_ID();
    
   
    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    $menu_list = '<ul class="side-nav" id="menu-' . $menu_name . '">';
    
    foreach ($menu_items as $key => $menu_item ) {
        $title = $menu_item->title;
        $url = $menu_item->url;
        $id = $menu_item->object_id;


        $class = ($current_page == $id) ? 'active' : '';
        $menu_list .= '<li class="' .$class.'"><a href="' . $url . '">' . $title . '</a></li>';
    }
    $menu_list .= '</ul>';
    echo '<nav>';
    echo '<h3>'.$menu->name.'</h3>';
    echo $menu_list;
    echo '</nav>';
}else{
    $terms = get_terms( $category_name, array(
        'hide_empty' => true,
    ) );
    echo '<nav>';
    echo '<h3>'. __('Categories','podium'). '</h3>';    
    echo '<ul class="side-nav">';    
    foreach($terms as $term){
        echo '<li>';
        echo '<a href="'. get_term_link($term->term_id).' "> '. $term->name. '</a>';
        echo '</li>';
        }
    echo '</ul>';
    echo '</nav>';
}
