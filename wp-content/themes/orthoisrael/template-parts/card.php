<div class="large-3 cell specialist-card mb-20" >
    <a href="<?php the_permalink(); ?>" data-equalizer-watch>

        <?php if(has_post_thumbnail()){
            the_post_thumbnail('thumbnail');
        }else{
            echo '<img src="' . get_template_directory_uri().'/dist/images/avatar.png">';
        }

        ?>

        <h3><?php the_title(); ?> </h3>
        <p> <?php  echo get_the_content_clean(14); ?></p>
        <i class="material-icons">add</i>
        <?php if(get_field('show_as_specialist')){ ?>
           <!-- <span class="spe-ribbon"><?php //_e('Specialist', 'podium'); ?></span> -->
        <?php } ?>
    </a>
</div>