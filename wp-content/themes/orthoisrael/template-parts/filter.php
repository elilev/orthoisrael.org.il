<?php  
$city_id = '';
$area_id = '';
    if(isset($_GET['city']) && $_GET['city'] != ''){
        $city_id = $_GET['city'];
    }
    if(isset($_GET['area']) && $_GET['area'] != ''){
        $area_id = $_GET['area'];
    }
?>
<form class="filter" id="filter" action="<?php echo get_the_permalink(pll_get_post(146)); ?>">
        <h4><?php _e('Search for Dentist/Specialist','podium'); ?></h4>
            <?php 
                $area_terms = get_terms( 'location', array(
                    'hide_empty' => true,
                ) );
            ?>
            <div class="grid-x grid-padding-x">
            <div class="large-auto small-12 cell">  
            <label><?php _e('Area','podium'); ?></label>
                <select  name="area"  id="areaselect">
                    <option value="all"><?php _e('All Areas','podium'); ?></option>
                    <?php foreach($area_terms as $area_term){
                        if($area_term->parent == 0){
                            $selected ='';
                            if($area_id == $area_term->term_id) $selected = 'selected';
                            echo '<option value="'.$area_term->term_id.'"' .$selected.' data-id="'.$area_term->term_id.'">'.$area_term->name .' </option> ';
                        }
                        }?>
                </select> 
            </div>  
            <div class="large-auto small-12 cell">  
            <label><?php _e('City','podium'); ?></label>
                <select  name="city" id="subareaselect">
                    <option value="all"><?php _e('All Cities','podium'); ?></option>
                    <?php foreach($area_terms as $area_term){
                            $selected ='';
                            if($city_id == $area_term->term_id) $selected = 'selected';
                            echo '<option value="'.$area_term->term_id.'" ' . $selected. ' data-parent="'.$area_term->parent.'">'.$area_term->name .' </option> ';
                       
                        }?>
                </select>
            </div>  
            <div class="large-auto small-12 cell position-relative"> 
            <label><?php _e('Free Search','podium'); ?></label>   
            <input type="text" placeholder="<?php _e('Enter a Specialist name','podium'); ?>"   id="specialistInput" autocomplete="off" data-id="">
            <input name="sn" type="hidden" value=""   id="specialistInputHidden">
            <ul class="" id="specialistsFullList">
            <?php 
                      $specialistsFullList = array();
                      $args = array(
                        'post_type' => 'specialist',
                        'posts_per_page' => '-1',
                      );
                        $specialist_query = new wp_query( $args );
                        if ($specialist_query->have_posts()) { 
                            while ($specialist_query->have_posts()) { $specialist_query->the_post();
                                $has_term ='all'; //default all locations
                                if ( $terms = get_the_terms( $post, 'location' ) ) {
                                  $has_term =array();
                                  foreach($terms as $term){
                                    array_push($has_term, $term->term_id);
                                  }
                                  $has_term =  implode(',', $has_term);
                 
                                }
                                $title = get_the_title();
                                $title =  str_replace('"', '"', $title );
                                $title =  str_replace('״', '"', $title );
                                echo '<li data-id="'.get_the_id().'"  data-area="'. $has_term.'" class="specialists-list-item">'.$title.'</li> ';
                            }
                        }
                       
                        wp_reset_query();
                        wp_reset_postdata();
                    ?> 
            </ul>

            </div>  
            <div class="large-auto small-12 cell">  
                <label>&nbsp;</label> 
                <?php if(is_front_page()){?>
                    <input class="button" type="submit" value="<?php _e('Search','podium'); ?>">
                <?php }else{ ?>
                
                    <button class="button filter-button"><?php _e('Search','podium'); ?></button>
                <?php } ?>
            </div>        
            </div>
        </form>