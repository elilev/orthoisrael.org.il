<?php 


if ( !isset($pagetitle) ) $pagetitle = get_field( 'page_title' );
if ( !isset($height) ) $height = 'short';

$URI= get_permalink();
?>

<div class="page-top <?php echo $height; ?>" data-animate="slide-in-down">
	<div class="grid-container narrow">
	<div class="grid-x grid-padding-x align-center">
		<div class="small-12 medium-12 cell">
		<?php if(function_exists('bcn_display')){
                echo '<div class="breadcrumbs">';        
                bcn_display();
                echo '</div>';
        }?>
			   <h1 class="title">
			   	<span>
				<?php 
				if($pagetitle){
					echo $pagetitle;
				}
				elseif( is_singular() ) {
					the_title();
				}
				elseif (is_home() && !is_front_page()) {
					single_post_title();
					
				}
				elseif( is_category() ) {
					$category = get_category( get_query_var( 'cat' ) );
					echo $category->name;
				}
				elseif( is_tax() ) {
				   $term = get_term_by( 'slug', get_query_var( 'term' ), 
				   get_query_var( 'taxonomy' ) ); 
				   echo $term->name;
				}
				else{
					the_title();
				}
				?>
				</span>
				</h1>
				<?php if(is_single() && get_field('show_as_specialist')){ ?>
					<h5><?php _e('Orthodontist','podium'); ?></h5>
        		<?php } ?>
				

		</div>
		
	</div>
	</div>
	
</div>

