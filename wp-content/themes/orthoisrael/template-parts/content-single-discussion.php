<?php
/**
 * Template part for displaying single posts.
 *
 * @package podium
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('m-40  pxy-2 discussion-single'); ?>>
	<div class="entry-content">
		<h2><?php the_title(); ?></h2>
		<div class="small-12 cell discussion-list-single">
			<div class="meta">
				<?php if( get_field('name') ){ ?>
					<span><?php _e('Posted by:','podium'); ?> <?php  echo the_field('name'); ?></span> <i class="material-icons">fiber_manual_record</i>
				<?php } ?>
				<span> <?php echo get_the_date('d.m.Y'); ?> </span>
			</div>
		</div>
			
		
		<?php the_field('content'); ?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->

