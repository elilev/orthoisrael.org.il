<?php
/**
 * Template part for displaying single posts.
 *
 * @package podium
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('m-40 mb-40 pxy-2'); ?>>
	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->

