const filter = document.getElementById("filter");

if(filter){

let selectFlag = false;
const inputField = document.getElementById('specialistInput');
const inputFieldHidden = document.getElementById('specialistInputHidden');
const specialistsFullList = document.getElementById('specialistsFullList');
const specialistsFullListItems = specialistsFullList.getElementsByTagName("li");
// search filter
$('#areaselect').on('change', function () {
    let termId;
    termId = $(this).val();
    if (termId == 'all') {
        $('#subareaselect option').each(() => {
            $(this).show();
        });
       
        hideSpecialist(termId);
    } else {
        hideSubCategory(termId);
        hideSpecialist(termId);
    }
    if (selectFlag) {
        $('#subareaselect').prop('selectedIndex', 0);
        $('#specialistselect').prop('selectedIndex', 0);
    }
    specialistsFullList.style.display ="none";
    inputFieldHidden.value = '';
    inputField.value = '';
    

    selectFlag = true; // flag is here to make prop NOT change on first+ time
});
// run on page load
$('#areaselect').trigger('change');

function hideSubCategory(termId) {
    $('#subareaselect option').each(function () {
        if ($(this).attr('data-parent') == termId) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}
function hideSpecialist(termId) {
    $('#specialistsFullList li').each(function () {
        const listOfLocations = $(this).data('area').toString();
        const listOfLocationsArray = listOfLocations.split(',');
        if (listOfLocationsArray.indexOf(termId) !== -1 || termId === 'all') {
            console.log(termId);
            $(this).show().removeClass('hide');
        } else {
            $(this).hide().addClass('hide');
        }
        
    });
}
// hide parent taxonomy terms
$('#subareaselect option').each(function () {
    if ($(this).attr('data-parent') == '0') {
        $(this).hide();
    }
});

// Filter listener
$('.filter-button').on('click', function (e) {
    if ($(this).hasClass('filter-home')) {
        return;
    } else {
        e.preventDefault();
        let area = $('#areaselect').val();
        let city = $('#subareaselect').val();
        let specialistID = inputFieldHidden.value;

        if (city === 'all') {
            getSpecialist(area, specialistID);
        } else {
            getSpecialist(city, specialistID);
        }
    }
});

// ajax - get specialist
function getSpecialist(termId, specialistID) {
    console.log(termId);
    console.log(specialistID);
    $('.filter-results').html(
        '<div class="loader-wrap"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
    );
    $.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php?action=get_specialist',
        data: {
            termId: termId,
            specialistID: specialistID,
        },

        success: function (data) {
            if (data) {
                $('.filter-results').html(data);
                console.log('success');
            } else {
                $('.filter-results').html('לא נמצאו תוצאות');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
        },
        complete: function () {
            //console.log('complete');
        },
    });
}


function filterResults(input){
    for (i = 0; i < specialistsFullListItems.length; i++) {
        let text =  specialistsFullListItems[i];
        text = text.textContent || text.innerText;
        if (text.indexOf(input) > -1) {
            if(!specialistsFullListItems[i].classList.contains('hide')){
                specialistsFullListItems[i].style.display = "block";
            }
            
        } else {
            specialistsFullListItems[i].style.display = "none";
        }

    }
}


    inputField.addEventListener("input", e =>{
        specialistsFullList.style.display ="block";
        let input =  e.target.value.replace('״', '"');
        filterResults(input);
        if(input === ''){
            specialistsFullList.style.display ="none";
            inputField.innerText = '';
        }
    })
    specialistsFullList.addEventListener("click", e =>{
        if (e.target.tagName === 'LI'){
            inputField.value = e.target.innerText;//set the text
            inputFieldHidden.value =  e.target.dataset.id; // set the value of field like the clicked item data-id
            specialistsFullList.style.display ="none"; // hide the list after click
            }
    });
}
    