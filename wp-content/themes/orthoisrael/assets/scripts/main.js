'use strict';

// Foundation initialization
// $('#commentform .submit').removeAttr('name');
// $('#commentform').attr('data-abide', '');
$(document).foundation();

$('.has-submenu > a').on('click', function (e) {
    e.preventDefault();
    toggleMobileMenu($(this));
});

$('.callout .close-button').click(function () {
    $(this).parent().slideUp();
});
function toggleMobileMenu(elm) {
    const allPanels = $('.off-canvas-list .submenu');

    const target = elm.next();
    if (target.hasClass('active')) {
        target.slideUp();
        target.removeClass('active');
    } else {
        if (allPanels.hasClass('active')) {
            allPanels.removeClass('active');
            allPanels.slideUp();
        }
        target.addClass('active').slideDown();
    }
    return false;
}

// display specialist form

$('#update-card-link').click(function (e) {
    e.preventDefault();
    $('.specialist-form').show();
    $('html, body').animate(
        { scrollTop: $('.specialist-form').offset().top },
        0
    );
});

$('#new-topic-button').click(function (e) {
    e.preventDefault();
    $('.new-topic-form').slideDown();
});
$('.reset-form').click(function () {
    $('.new-topic-form').slideUp();
});

$('.specialist-list-select').on('change', function () {
    var element = $(this).find('option:selected');
    $('#author').val(element.val());
    $('#email').val(element.attr('data-email'));
});

$('.hero-slider').slick({
    infinite: true,
    speed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    pauseOnHover: false,
    rtl: true,
    fade: true,
    prevArrow: $('.arrow-prev'),
    nextArrow: $('.arrow-next'),
});

$('.specialist-slider').slick({
    infinite: true,
    speed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 800,
    arrows: true,
    pauseOnHover: false,
    rtl: true,
    prevArrow: $('.arrow-prev-spe'),
    nextArrow: $('.arrow-next-spe'),
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            },
        },
    ],
});

$('.news-slider').slick({
    infinite: true,
    speed: 4000,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 0,
    arrows: false,
    pauseOnHover: false,
    vertical: true,
    cssEase: 'linear',
    infinite: true,
    adaptiveHeight: true,
    centerMode: true,
});

// counting numbers

let flag = true;
$(document).scroll(function (evt) {
    let elm = $('#hp-counter');
    if (elm.length > 0) {
        let elementPos = Math.abs(
            $('#hp-counter').position().top - $(window).height() / 2
        );
        let scrollTop = $(this).scrollTop();
        if (scrollTop > elementPos - 200) {
            if (flag) {
                runCounter();
            }
        }
    }
});

function runCounter() {
    $('.number').each(function () {
        let $this = $(this);
        let cssObj = {
            Counter: 0,
        };
        jQuery({ cssObj }).animate(
            { Counter: $this.attr('data-stop') },
            {
                duration: 4000,
                easing: 'swing',
                step: function (now) {
                    $this.text(Math.round(now));
                },
                complete: function (now) {
                    $this.text($this.attr('data-stop'));
                },
            }
        );
    });
    flag = false;
}

// recapcha
$('#new-topic-form').submit(function (event) {
    event.preventDefault();

    grecaptcha.ready(function () {
        grecaptcha
            .execute('6LclMCQbAAAAAFPrwFR6ujR1xJyWLBJq0aUVr4SG', {
                action: 'newTopic',
            })
            .then(function (token) {
                $('#new-topic-form').prepend(
                    '<input type="hidden" name="token" value="' + token + '">'
                );
                $('#new-topic-form').prepend(
                    '<input type="hidden" name="action" value="newTopic">'
                );
                $('#new-topic-form').unbind('submit').submit();
            });
    });
});
