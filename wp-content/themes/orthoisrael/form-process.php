<?php
require_once('../../../wp-load.php');
define("RECAPTCHA_V3_SECRET_KEY", '6LclMCQbAAAAAM_WHgEXSSTl7oYG9h4T47df-X1-');

if ( isset( $_POST['submit-new-topic'] )) {

$token = $_POST['token'];
$action = $_POST['action'];

//call curl to POST request
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => RECAPTCHA_V3_SECRET_KEY, 'response' => $token)));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);
$arrResponse = json_decode($response, true);

	// verify the response
	if($arrResponse["success"] == '1' && $arrResponse["action"] == $action && $arrResponse["score"] >= 0.5) {	
		$topic     = sanitize_text_field($_POST['topic']);
		$author     = sanitize_text_field($_POST['author']);
		$content     = sanitize_textarea_field($_POST['content']);
		$email     = sanitize_email($_POST['email']);
		$term     = clean($_POST['term']);
		$url     = clean($_POST['url']);
		$recipients     = $_POST['recipients'];


		$post = array(
			'post_name'      => $topic,
			'post_title'     => $topic,
			'post_status'    => 'draft',
			'post_type'      => 'discussion',

		);

		if( !isset($_POST['postid'])){ // if did not add a page already
			$pid = wp_insert_post( $post );  // add post

			// update post name to id
			$my_post = array(
				'post_name'		=> $topic, // The name (slug) for your post
				'ID'			=> $pid,
				'post_title'	=> $topic,
				'post_status'    => 'publish',
			);
			wp_update_post( $my_post );

		}

		else {
			$pid = $_POST['postid'];
		}

		
		update_field( "field_603530615f9b7", $content, $pid );
		update_field( "field_603cb2d57fa1f", $author, $pid );
		update_field( "field_603cb32a7fa20", $email, $pid );

		wp_set_post_terms($pid,$term, 'discussion-type');
		$full_url = get_the_permalink($pid) . '/?res=1';
		
		  
		//send email
		$subject = 'נושא חדש בפורום ';
		$mail = '<h3>נושא חדש בפורום</h3>';
		$mail .='<table>';
		$mail .='<tr><td>נושא: </td><td>'.$topic .'</td></tr>';
		$mail .='<tr><td>שם: </td><td>'.$author .'</td></tr>';
		$mail .='<tr><td>קישור מלא: </td><td>'.get_the_permalink($pid) .'</td></tr>';
		$mail .='<tr><td>תוכן: </td><td>'.$content .'</td></tr>';
		$mail .='</table>';
		
		$headers = array('Content-Type: text/html; charset=UTF-8', 'From: Orthoisrael <noreply@orthoisrael.org.il> ');
		
		//array_push($recipients , 'eliyahu.lev@gmail.com');
		$to = implode(', ', $recipients);

		wp_mail( $to, $subject, $mail, $headers);
		
	} else {
		// spam submission
		// show error message
		$full_url = site_url(). '/' .$url . '/?res=2';
	}


} // end if submit check	
else{
    $full_url = site_url(). '/' .$url . '/?res=2';
}
header('Location:' . $full_url);
















