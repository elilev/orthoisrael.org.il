<?php
/**
 *
 * @package podium
 */
use Podium\Config\Settings as settings;

$settings = new settings();

get_header();
$height = 'tall';
$pagetitle =   sprintf(esc_html__('Search Results for: %s', 'podium'), '<span>"' . get_search_query() . '"</span>');
include( locate_template( 'template-parts/page-top.php', false, false ) );
?>

<div class="grid-container search-page">
<div id="content" class="site-content grid-x grid-padding-x">
<div id="primary" class="content-area offset small-12 large-12 cell">
    <main id="main" class="site-main" role="main">
    <div class="grid-x grid-padding-x">
        <div class="large-4 cell side-bar">
            <?php  get_template_part('template-parts/sidebar-menu'); ?>    
        </div>
        <div class="large-8 cell">
        <div class="content-block"> 
   

        <?php

          while (have_posts()) { the_post(); ?>
              <?php get_template_part('template-parts/content', 'search'); ?>
        <?php } ?>
      
        </div>
        <div class="row">
                <div class="grid-x grid-margin-x align-center text-center">
                    <div class="large-10 cell"><?php   podium_pagination(); ?></div>
                </div>
        </div>
        </div>
        
    </div>           
</main><!-- #main -->
</div><!-- #primary -->

</div><!-- #content -->
</div><!-- .grid-container -->
<?php get_footer();
